#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

/**
 * @brief loadAndDisplay        Testowanie kilku roznych sposobow wczytywania obrazu.
 */
void loadAndDisplay()
{
    // Wczytaj obraz z pliku, sciezka: "../data/wm_lab_1/gandalf.png"

    // Wyswietl obraz

}

/**
 * @brief funWithContrast       Dokonaj zmiany kontrastu wczytanego obrazu
 */
void funWithContrast(int alfa, int beta)
{
    // Wczytanie obrazu testowego

    // Dokonanie zmiany kontrastu
 
    // Wyswietlenie
}

/**
 * @brief funWithThreshold      Dokonaj operacji binaryzacji.
 */
void funWithThreshold(int val)
{
    // Wczytywanie obrazu testowego

    // Binaryzacja

    // Wyswietlenie

}

/**
 * @brief funWithMorph          Przyklad wykorzystania operacji morfologicznych.
 */
void funWithMorph(int kernel_size)
{
    // Wczytywanie obrazu testowego

    // Dylatacja

    // Erozja

    // Wyswietlenie

}

/**
 * @brief funWithFiltering      Przyklad filtracji obrazu.
 */
void funWithFiltering(int kernel_size)
{
    // Wczytywanie obrazu testowego

    // Filtr medianowy

    // Filtr gaussowski

    // Wyswietlenie

}

/**
 * @brief funWithMasks          Definicja maski oraz proste dzialania z jej wykorzystaniem.
 */
void funWithMasks()
{    
    // Wczytywanie obrazu testowego

    // Tworzenie maski

    // Ustaw okrąg (610, 390), 100

    // Skopiuj zdjęcie i przemaskuj

    // Wyświetlanie

}

/**
 * @brief funWithPixelIteration Iteracja po pixelach (rysowanie przekatnych).
 */
void funWithPixelIteration()
{
    // Wczytywanie obrazu testowego

    // Przekreśl obraz

    // Wyświetlanie
}

int main(int argc, char** argv)
{
    loadAndDisplay();
    funWithContrast(2, -40);
    funWithThreshold(55);
    funWithMorph(7);
    funWithFiltering(7);
    funWithMasks();
    funWithPixelIteration();
    return 0;
}
