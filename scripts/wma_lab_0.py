import cv2
import numpy as np

if __name__ == "__main__":
    print("CV_MAJOR =", cv2.__version__.split('.')[0])
    print("CV_MINOR =", cv2.__version__.split('.')[1])
    print("============= TEST PASSED =============")
