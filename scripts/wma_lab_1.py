import cv2
import numpy as np

def loadAndDisplay():
    # Wczytaj obraz z pliku, sciezka: "../data/wm_lab_1/gandalf.png"i
    
    # Wyswietl obraz
    pass

def funWithContrast(alfa, beta):
    # Wczytanie obrazu testowego

    # Dokonanie zmiany kontrastu

    # Wyświetlenie
    pass

def funWithThreshold(val):
    # Wczytanie obrazu testowego

    # Binaryzacja

    # Wyświetlenie
    pass

def funWithMorph(kernel_size):
    # Wczytanie obrazu testowego

    # Dylatacja

    # Erozja

    # Wyświetlenie
    pass

def funWithFiltering(kernel_size):
    # Wczytanie obrazu testowego

    # Filtr medianowy

    # Filtr gaussowski

    # Wyświetlenie
    pass

def funWithMasks():
    # Wczytanie obrazu testowego

    # Tworzenie maski

    # Ustaw okrąg (610, 390), 100

    # Skopiuj zdjęcie i przemaskuj

    # Wyświetlenie
    pass

def funWithPixelIteration():
    # Wczytanie obrazu testowego

    # Przekreśl obraz

    # Wyświetlenie
    pass

if __name__ == "__main__":
    loadAndDisplay()
    funWithContrast(2, -40)
    funWithThreshold(55)
    funWithMorph(7)
    funWithFiltering(7)
    funWithMasks()
    funWithPixelIteration()
